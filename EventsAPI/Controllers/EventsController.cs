﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventCore;
using EventCore.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace EventsAPI.Controllers
{
    [Route("api/[controller]")]
    public class EventsController : Controller
    {
        private IEventRepository _repo;

        public EventsController(IEventRepository repo)
        {
            _repo = repo ?? throw new ArgumentNullException(nameof(repo));
        }
        [HttpGet]
        public IActionResult Get(int page = 1, int itemsPerPage = 12)
        {
            var model = _repo.LoadAll(page, itemsPerPage);
            return Ok(model);
        }
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var model = _repo.Load(id);
            return Ok(model);
        }
    }
}
