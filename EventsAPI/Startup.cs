﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventCore;
using EventCore.Repositories;
using EventData;
using EventData.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

namespace EventsAPI
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EventsDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("EventsConnStr")));
            services.AddScoped(typeof(IUnitOfWork), typeof(UnitOfWork));
            services.AddScoped(typeof(IEventRepository), typeof(EventRepository));
            services.AddCors();
            services.AddMvc();
        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors(builder => builder.AllowAnyOrigin()
                                          .AllowAnyHeader()
                                          .AllowAnyMethod());
            app.UseMvc();
            Seed(app);
        }
        private void Seed(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var images = new List<EventImage>()
                {
                    new EventImage() { Url = "http://partyalicante.com/wp-content/uploads/2015/04/party-Alicante.jpg" },
                    new EventImage() { Url = "https://ncache.ilbe.com/files/attach/new/20151122/1349949/945537964/6981533708/07adfc9a9263e04f7b69ddd9752264ae.jpg" },
                    new EventImage() { Url =  "http://www.travelwith.jp/lp/seo/20140913/images/10629761_782646651758226_2852199739728696629_n.jpg" }
                };
                var participants = new List<EventParticipant>()
                {
                    new EventParticipant() { Name = "Beqa" },
                    new EventParticipant() { Name = "Martin"}
                };

                var db = serviceScope.ServiceProvider.GetService<EventsDbContext>();
                db.Database.EnsureCreated();
                if (!db.Events.Any())
                {
                    db.Events.Add(new Event()
                    {
                        Descriptions = new List<EventDescription>() {
                            new EventDescription() { Text = "Event1 Desc 1" },
                            new EventDescription() { Text = "Event1 Desc 2" }
                        },
                        Name = "Event 100",
                        Images = images,
                        Schedule = new List<EventSchedule>()
                        {
                            new EventSchedule() { Date = DateTime.Now.AddDays(1),
                                Participants = participants
                            }
                        }
                    });
                    db.Events.Add(new Event()
                    {
                        Descriptions = new List<EventDescription>() {
                            new EventDescription() { Text = "Event2 Desc 1" },
                            new EventDescription() { Text = "Event2 Desc 2" }
                        },
                        Name = "Event 20",
                        Images = new List<EventImage>()
                        {
                            new EventImage() { Url =  "http://cbw.ge/wp-content/uploads/2015/06/jazz-600x330.jpg" }
                        },
                        Schedule = new List<EventSchedule>()
                        {
                            new EventSchedule() { Date = DateTime.Now.AddDays(2),
                                Participants = new List<EventParticipant>()
                                {
                                    new EventParticipant() { Name = "Beqa" }
                                }
                            },
                            new EventSchedule() { Date = DateTime.Now.AddDays(3),
                                Participants = new List<EventParticipant>()
                                {
                                    new EventParticipant() { Name = "Martin"}
                                }
                            }
                        }
                    });
                    for (int i = 0; i < 15; i++)
                    {
                        var image = images.ToList().ElementAt(new Random().Next(images.Count()));
                        var user = participants.ElementAt(new Random().Next(participants.Count()));
                        db.Events.Add(new Event()
                        {
                            Name = $"Event {i}",
                            Descriptions = new List<EventDescription>() { new EventDescription() { Text = $"description {i}" } },
                            Images = new List<EventImage>() { new EventImage() { Url = image.Url } },
                            Schedule = new List<EventSchedule>()
                            {
                                new EventSchedule() { Date = DateTime.Now.AddHours(new Random().Next(100)),
                                    Participants = new List<EventParticipant>() {
                                        new EventParticipant() { Name = user.Name }
                                    }
                                }
                            }
                        });
                    }
                    db.SaveChanges();
                }
            }
        }
    }
}
