﻿using System;
using System.Collections.Generic;
using System.Text;
using EventCore;
using Microsoft.EntityFrameworkCore;

namespace EventData
{
    public class EventsDbContext : DbContext
    {
        public DbSet<Event> Events { get; set; }
        public EventsDbContext(DbContextOptions options) : base(options) { }
    }
}
