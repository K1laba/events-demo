﻿using System;
using System.Collections.Generic;
using System.Text;
using EventCore;
using EventCore.Repositories;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace EventData.Repositories
{
    public class EventRepository : IEventRepository
    {
        private IUnitOfWork _uow;
        private EventsDbContext _ctx;

        public EventRepository(IUnitOfWork uow)
        {
            _uow = uow ?? throw new ArgumentNullException(nameof(uow));
            _ctx = _uow.GetDbContext() as EventsDbContext;
        }
        public Event Load(Guid id)
        {
            return Events().SingleOrDefault(e => e.Id == id);
        }
        public EventListResult LoadAll(int currentPage, int itemsPerPage)
        {
            var events = Events().Skip((currentPage - 1) * itemsPerPage).Take(itemsPerPage).ToList();
            var result = new EventListResult()
            {
                Events = events,
                PagerInfo = new Pager(itemsPerPage, Events().Count(), currentPage)
            };
            return result;
        }
        private IIncludableQueryable<Event, IEnumerable<EventParticipant>> Events()
        {
            return _ctx.Events.AsNoTracking().Include(e => e.Descriptions)
                                  .Include(e => e.Images)
                                  .Include(e => e.Schedule).ThenInclude(s => s.Participants);
        }
    }
}
