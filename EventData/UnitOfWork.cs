﻿using System;
using System.Collections.Generic;
using System.Text;
using EventCore;

namespace EventData
{
    public class UnitOfWork : IUnitOfWork
    {
        private EventsDbContext _ctx;

        public UnitOfWork(EventsDbContext context)
        {
            _ctx = context ?? throw new ArgumentNullException(nameof(context));
        }
        public void Commit()
        {
            _ctx.SaveChanges();
        }

        public object GetDbContext()
        {
            return _ctx;
        }
    }
}
