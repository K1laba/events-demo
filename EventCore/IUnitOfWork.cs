﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventCore
{
    public interface IUnitOfWork
    {
        object GetDbContext();
        void Commit();
    }
}
