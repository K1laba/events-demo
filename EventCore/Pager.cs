﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventCore
{
    public class Pager
    {
        public int ItemsPerPage { get; private set; }
        public int TotalItems { get; private set; }
        public int CurrentPage { get; private set; }
        public Pager(int itemsPerPage, int totalItems, int currentPage)
        {
            ItemsPerPage = itemsPerPage;
            TotalItems = totalItems;
            CurrentPage = currentPage;
        }
    }
}
