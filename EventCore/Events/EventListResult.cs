﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventCore;

namespace EventCore
{
    public class EventListResult
    {
        public IEnumerable<Event> Events { get; set; }
        public Pager PagerInfo { get; set; }
    }
}
