﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventCore
{
    public class EventSchedule : BaseEntity
    {
        private IEnumerable<EventParticipant> _participants = null;
        public DateTime Date { get; set; }
        public IEnumerable<EventParticipant> Participants
        {
            get { return _participants == null ? _participants = new List<EventParticipant>() : _participants; }
            set { _participants = value; }
        }
    }
}
