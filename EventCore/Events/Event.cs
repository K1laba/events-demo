﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace EventCore
{
    public class Event : BaseEntity
    {
        private IEnumerable<EventSchedule> _schedule = null;
        private IEnumerable<EventImage> _images = null;
        private IEnumerable<EventDescription> _descriptions = null;

        public string Name { get; set; }
        public DateTime StartDate { get { return this.Schedule.Min(i => i.Date); } }
        public DateTime EndDate { get { return this.Schedule.Max(i => i.Date); } }
        public int TotalParticipants { get { return this.Schedule.SelectMany(s => s.Participants).Count(); } }
        public IEnumerable<EventDescription> Descriptions
        {
            get { return _descriptions == null ? _descriptions = new List<EventDescription>() : _descriptions; }
            set { _descriptions = value; }
        }
        public IEnumerable<EventSchedule> Schedule
        {
            get { return _schedule == null ? _schedule = new List<EventSchedule>() : _schedule; }
            set { _schedule = value; }
        }
        public IEnumerable<EventImage> Images
        {
            get { return _images == null ? _images = new List<EventImage>() : _images; }
            set { _images = value; }
        }
    }
}
