﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventCore
{
    public class EventDescription : BaseEntity
    {
        public string Text { get; set; }
    }
}
