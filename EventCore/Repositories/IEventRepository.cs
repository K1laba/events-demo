﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventCore.Repositories
{
    public interface IEventRepository
    {
        Event Load(Guid id);
        EventListResult LoadAll(int currentPage, int itemsPerPage);
    }
}
