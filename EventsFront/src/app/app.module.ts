import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { AlertModule } from 'ng2-bootstrap';
import { EventsComponent } from './events/events.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { EventsService } from './events/events.service';
import { HeaderComponent } from './header/header.component';
import { PagerComponent } from './pager/pager.component';
import { Config } from './config';

const routes: Routes = [
  {
    path: '',
    component: EventsComponent
  }, 
  {
    path: 'event/:id',
    component: EventDetailComponent
  }];

@NgModule({
  declarations: [
    AppComponent,
    EventsComponent,
    EventDetailComponent,
    HeaderComponent,
    PagerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlertModule.forRoot(),
    RouterModule.forRoot(routes)
  ],
  providers: [EventsService, Config],
  bootstrap: [AppComponent]
})
export class AppModule { }
