import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Event } from './event.model';
import { Config } from '../config';
@Injectable()
export class EventsService {
    protected headers:Headers;
    constructor(protected http:Http, private config:Config) { 
        
    }
    get(page: number): Promise<any> {
        return this.http.get(this.config.apiUrl + "/events/?page=" + page)
                        .toPromise().then(r => r.json())
                                    .catch(() => {});
    }
    getSingle(id: string): Promise<Event> {
        return this.http.get(this.config.apiUrl + '/events/' + id)
                        .toPromise().then(r => r.json())
                                    .catch(() => {});
    }
}