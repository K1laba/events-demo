import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Event } from './event.model';
import { EventsService } from './events.service';
import { Pager } from '../pager/pager.model';
@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  private events: Array<Event>;
  private pager: Pager;
  private pageNumber: number;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private eventService: EventsService) { }

  ngOnInit() {
    this.pageNumber = +this.getQuery("page") || 1
    this.load();
  }
  load(): void {
    this.eventService.get(this.pageNumber).then(r => {
      this.events = r.events;
      this.pager = r.pagerInfo;
    });
  }
  navigate(): void {
    this.router.navigate(['/'], { queryParams: { page: this.pageNumber } });

    setTimeout(() => {
      this.load();
    }, 0);
  }
  setPage(page): void {
    this.pageNumber = page;
    this.navigate();
  }
  private getQuery(key: string): string {
        var result = "";
        this.activatedRoute.queryParams.forEach((p: Params) => {
            result = p[key];
        });
        return result;
    }
  goToDetails(id: string): void {
    var link = ["event", id];
    this.router.navigate(link);
  }

}
