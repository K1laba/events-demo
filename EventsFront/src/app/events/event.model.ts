class BaseModel {
    id: string;
}
export class Event extends BaseModel {
    name: string;
    startDate: Date;
    endDate: Date;
    images: Array<EventImage>;
    descriptions: Array<EventDescription>;
    participants: Array<EventParticipant>;
}
class EventDescription extends BaseModel {
    text: string;
}
class EventParticipant extends BaseModel {
    name: string
}
class EventImage extends BaseModel {
    url: string;
}