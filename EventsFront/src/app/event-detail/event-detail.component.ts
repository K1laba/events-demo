import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Event } from '../events/event.model';
import { EventsService } from '../events/events.service';
@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {

  private event:Event;
  constructor(private eventService: EventsService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.forEach((p:Params) => {
            let id = p['id'];
            this.eventService.getSingle(id).then(r => this.event = r);
        });
  }

}
