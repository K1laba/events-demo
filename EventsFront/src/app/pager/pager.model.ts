export class Pager {
    constructor(public currentPage: number, public itemsPerPage:number, public totalItems: number) {}
}