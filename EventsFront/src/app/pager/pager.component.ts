import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Pager } from './pager.model';
@Component({
    selector: 'my-pager',
    templateUrl: './pager.component.html'
})
export class PagerComponent {
    pager: Pager;
    @Input() set setPager(value:Pager) {
        this.pager = value;
        this.calculate();
    }
    @Output() onPageChanged = new EventEmitter<number>();
    canShowPrevDots: Boolean;
    canShowNextDots: Boolean;
    pageNumbers: Array<Number> = [];
    pagesCount: number;
    
    constructor() {
        this.calculate();
    }
    setPage(page: number) {
        this.onPageChanged.emit(page);
    }
    calculate(): void {
        if (!this.pager) {return;}
        this.pagesCount = Math.ceil(this.pager.totalItems / this.pager.itemsPerPage);
        if (this.pagesCount <= 1 || this.pager.currentPage > this.pagesCount) { return; }

        var pagesFromCurrentPage = 1;
        this.canShowPrevDots = this.pager.currentPage >= pagesFromCurrentPage + 3;
        this.canShowNextDots = this.pager.currentPage <= this.pagesCount - pagesFromCurrentPage - 2;

        var startIndex = Math.max(2, this.pager.currentPage - pagesFromCurrentPage);
        var endIndex = Math.min(this.pagesCount - 1, this.pager.currentPage + pagesFromCurrentPage);
        this.pageNumbers = [];
        for(let i=startIndex; i<=endIndex; i++) {
            this.pageNumbers.push(i);
        }
    }
}