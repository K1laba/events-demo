import { EventsFrontPage } from './app.po';

describe('events-front App', () => {
  let page: EventsFrontPage;

  beforeEach(() => {
    page = new EventsFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
